# iverilog-v简介

修改的iverilog源码相比源码增加了一个新的组件： **tgt-json**组件，iverilog源码除了默认输出vvp文件还支持输出 null,fpga,vhdl类型的文件输出。具体信息参考iverilog说明文档。由于后期的开发把verilog源码转成vvp文件的时候，vvp的文件行数有时候会达到数百万行因此读取的速度较慢。为了提高iverilog后期处理数据的工作效率需要对iverilog的源码进行改写。根据[iverilog的官方开发文档](https://github.com/steveicarus/iverilog/blob/master/developer-quick-start.txt)可以知道在iverilog源码当中 /tgt-vvp组件的作用就是生成vvp文件。因此对iverilog源码的改写的主要思路就是参考 /tgt-vvp组件对其进行改写。

## 使用说明

- 编译

    对于修改后的iverilog，为了保证与源码的兼容性因此同时修改了相应的预编译文件。因此编译iverilog-v的操作与iverilog的操作步骤一样，具体操作步骤请参考[iverilog官方操作网站](https://iverilog.fandom.com/wiki/Installation_using_MSYS2)。

- 使用（使用tgt-json输出json数据)

    由于在iverilog源码上面新加了一个 tgt-json输出json格式的组件，在开发组件的时候同样为了保证与源码的兼容性，也改了源码自带的说明文档预编译文件。因此如果想让iverilog输出json格式数据，可以参考编译安装完后的安装目录下的iverilog.pdf说明文档。如要编译verilog源码转化为json数据格式，可以使用如下命令：

```shell
iverilog -t json  -o "add.json" add_tb.v add.v
```

其中的命令行参数 `-o`, `-t`的含义请参考安装目录下的iverilog.pdf

## 联系方式

邮箱： slive1024@163.com

代码地址： https://good.ncu.edu.cn/gitlab/pengxin/iverilog-ncu/-/tree/3-iverilog-v



# 研发工作


分析了iverilog源码当中的tgt-vvp组件我们可以知道，为了提取有效的json数据可以在原tgt-vvp组件上面进行修改。首先我们知道在原iverilog当中vvp文件主要是含由执行器路径,头语句，功能语句，注释，文件列表这几大部分组成。但是由于后期项目处理的需求需要输出的json文件包含主要逻辑门的信息，因此我们开发tgt-json的主要思路就是找出tgt-vvp组件当中相应的输出语句，把这些输出语句的相应程序修改把要输出到文件上面的信息存入到自己定义的数据结构当中去。最后利用开源的cJSON库导出输出成json类型的数据。

##  对tgt-vvp组件的改写工作

### 源码分析

对于第一阶段的研发要求，我们需要提取vvp文件信息当中的部分功能语句，这些功能语句为三大类： `.functor`, `.var`,`.net`，我们需要提取vvp文件里面包含这三大类关键信息的语句。为此我们在源码当中寻找有关这三大类的信息输出相应的代码。经过分析 /tgt-vvp 代码可以知道vvp文件的输出就是C语言里面常见的对文件指针操作。

#### 定义文件指针

```c
/* 
*	tgt-vvp/vvp.c
*/

FILE*vvp_out = 0;
int vvp_errors = 0;
unsigned show_file_line = 0;C
```

根据上面从源码当中摘出来的代码，我们可以知道在tgt-vvp组件当中定义了一个文件指针，这个文件指针就是用于输出vvp语句的文件指针，后面大部分的操作都是针对这个`vvp_out`文件指针进行操作。

#### 输出vvp语句代码案例

```c
/*
*	tgt-vvp/draw_delay.c
*/

static char* draw_const_net(void*ptr, char*suffix, uint64_t value)
{
      char tmp[64];
      char c4_value[69];
      unsigned idx;
      c4_value[0] = 'C';
      c4_value[1] = '4';
      c4_value[2] = '<';
      for (idx = 0; idx < 64; idx += 1) {
	    c4_value[66-idx] = (value & 1) ? '1' : '0';
	    value >>= 1;
      }
      c4_value[67] = '>';
      c4_value[68] = 0;

	/* Make the constant an argument to a BUFT, which is
	   what we use to drive the value. */
      fprintf(vvp_out, "L_%p/%s .functor BUFT 1, %s, C4<0>, C4<0>, C4<0>;\n",
	      ptr, suffix, c4_value);
      snprintf(tmp, sizeof tmp, "L_%p/%s", ptr, suffix);
      return strdup(tmp);
}

```

上面是输出一个`.functor`vvp语句的代码，由于我们的工作是从其中摘出这一句信息因此我可以不用考虑这些信息是如何进行处理的，只需要对上面语句的这一行代码进行数据处理，把fprintf要输出的信息存储为我们自己的数据结构再转成json类型的文本文件就可以了。

```c
fprintf(vvp_out, "L_%p/%s .functor BUFT 1, %s, C4<0>, C4<0>, C4<0>;\n", ptr, suffix, c4_value);
```

### tgt-json组件开发

#### 从tgt-vvp组件中提取出需要的信息

经过上面的源码分析，我们的主要目的就是如何把`sprintf`当中的信息取出来存储到我们自己需要的信息。对于`.functor`的vvp语句，我们一般分成四大部分：label, opercode, operand, statements。对于`.var`和`.net`我们可以大致分statements, label, name三大类。因此，对于从`fprintf`提取的信息我们可以用C语言当中的`sprintf`函数来完成。比如对于上面输出的`.functor`代码我们可以先申请相应的内存空间进行存储，存储到自己申请的内存空间当中去，修改代码如下：

```c
fprintf_selfmade(vvp_out, "L_%p/%s .functor BUFT 1, %s, C4<0>, C4<0>, C4<0>;\n",ptr, suffix, c4_value);

/*-------------------------- code ------------------------------------*/
char* label_ss = (char*)malloc(SIZE_DATA*sizeof(char));
char* operand_ss = (char*)malloc(SIZE_DATA*sizeof(char));
char* statements_ss = (char*)malloc(SIZE_DATA*sizeof(char));

sprintf(label_ss, "L_%p/%s", ptr, suffix);
sprintf(operand_ss, ", %s, C4<0>, C4<0>, C4<0>;", c4_value);

sprintf(statements_ss, "L_%p/%s .functor BUFT 1, %s, C4<0>, C4<0>, C4<0>;",ptr, suffix, c4_value);

label_ss = NULL;
operand_ss = NULL;
statements_ss = NULL;

free(label_ss);
free(operand_ss);
free(statements_ss);

```

**注：更加详细的代码可以参考 iverilog-v/tgt-json/draw_delay.c 源文件代码**

#### 存储提取出来的关键信息

对于提取出来的信息，考虑到后期用iverilog-v可能编译一个大型的verilog源码，产生的vvp文件可能达到上百万行因此就需要考虑存储信息的时候要尽可能地减少所占用的内存资源。为此经过调研分析我们打算采用单向链表的方式来存储提取出来的主要数据信息。为此，我们在 iverilog-v/tgt-json/vvp_priv.h声明了单向链表的操作函数，这些函数的实现代码在 iverilog-v/tgt-json/operate-json.c 中。例如对于`.functor`数据的存储链表操作函数声明如下：

```c
void Read_ListData(void);				// 读取链表函数
void Cell_DeleteList(void);				// 删除链表函数
unsigned int  Cell_GetListSize(void);	// 获取链表大小的函数
void Cell_AddNode(char* label, const char* opcode, char* type, char* operand, char* statements);		// 给链表添加一个结点
```

#### 输出json

由于我们需要的是vvp文件当中功能语句的`.functor`，`.var`, `.net`语句信息，当把数据存储到各自的单向链表之后则需要把数据转化成json格式进行输出。对于把数据输出成json文件，我们采用开源的cJSON库把链表存取的数据导出为json格式。

>​	[JSON](http://www.json.org/json-zh.html)(JavaScript Object Notation) 是一种轻量级的数据交换格式。易于人阅读和编写，同时也易于机器解析和生成。它基于JavaScript（Standard ECMA-262 3rd Edition - December 1999）的一个子集。 JSON采用完全独立于语言的文本格式，但是也使用了类似于C语言家族的习惯（包括C, C++, C#, Java, JavaScript, Perl, Python等）。这些特性使JSON成为理想的数据交换语言。（来自“开源中国”资料）。
>
>​	cJSON从名字可知，整个项目都是以极标准的C来写的，意思说，可以跨各种平台使用了。 [cJSON](http://sourceforge.net/projects/cjson/) 是一个超轻巧，携带方便，单文件，简单的可以作为ANSI-C标准的JSON解析器。

由上述引用资料可以知道，对于json的输出我们利用库就可以。因此我们需要做的就是在读取单向链表取出信息的时候调用cJSON库然后导出到指定的文件就可以，下面的代码是读取链表到调用cJSON导出json到指定文件的函数代码。可参考 iverilog-v/tgt-json/operate-json.c 。

```c
// 读取操作函数
void Read_ListData(void)
{
    
    cJSON *arry_elements = cJSON_CreateArray();
    cJSON *root = cJSON_CreateObject();


    Cells_Node *node = cell_head;
    char* ptr = NULL;

    if (node == NULL)
    {
        printf("List is empyt！\n");
    }
    else
    {
        for (int index = 0; index < Cell_GetListSize(); index++)
        {
            cJSON *elements;
            elements = cJSON_CreateObject(); 

            cJSON_AddStringToObject(elements, "statements:", node->statements);
            cJSON_AddStringToObject(elements, "label", node->cells.label);
            cJSON_AddStringToObject(elements, "type", node->cells.type);
            cJSON_AddStringToObject(elements, "opcode", node->cells.opcode);
            cJSON_AddStringToObject(elements, "operand", node->cells.operand);

            cJSON_AddItemToArray(arry_elements, elements);
            node = node->next;
        }
    }

    cJSON_AddItemToObject(root, "cells", arry_elements);


/*---------------------.var----------------------------------------------------*/

    Var_Data *node_var = var_head;
    cJSON *arry_elements_var = cJSON_CreateArray();

    if (node_var == NULL)
    {
        printf("List is empyt！\n");
    }
    else
    {
        for (int index = 0; index < Var_GetListSize(); index++)
        {

            cJSON *elements_var;
            elements_var = cJSON_CreateObject(); 

            cJSON_AddStringToObject(elements_var, "statements:", node_var->statements);
            cJSON_AddStringToObject(elements_var, "label:", node_var->label);
            cJSON_AddStringToObject(elements_var, "opcode:", node_var->opcode);
            cJSON_AddStringToObject(elements_var, "name:", node_var->name);

            cJSON_AddItemToArray(arry_elements_var, elements_var);
            node_var = node_var->next;
        }
    }

    cJSON_AddItemToObject(root, "variable", arry_elements_var);


/*---------------------------------- .net -----------------------------------------*/
    Net_Data *node_net = net_head;
    cJSON *arry_elements_net = cJSON_CreateArray();

    if (node_net == NULL)
    {
        printf("List is empyt！\n");
    }
    else
    {
        for (int index = 0; index < Net_GetListSize(); index++)
        {

            cJSON *elements_net;
            elements_net = cJSON_CreateObject(); 

            cJSON_AddStringToObject(elements_net, "statements:", node_net->statements);
            cJSON_AddStringToObject(elements_net, "label:", node_net->label);
            cJSON_AddStringToObject(elements_net, "opcode:", node_net->opcode);
            cJSON_AddStringToObject(elements_net, "name:", node_net->name);

            cJSON_AddItemToArray(arry_elements_net, elements_net);
            node_net = node_net->next;
        }
    }
    
    cJSON_AddItemToObject(root, "net", arry_elements_net);
    
    ptr = cJSON_Print(root);
    fprintf(vvp_out_JSON, ptr);
    cJSON_Delete(root);

}
```

该函数实现了`.functor`, `.var`， `.net`数据读取已经调用cJSON生成json到文件的全过程。

#### 对于tgt-vvp组件下vvp.c文件的分析

前面已经完成了信息的提取，导出但是为了降低开发的难度我们需要将自己写的代码嵌入到源码当中。下面是从源码中摘出的几个代码库进行分析，至于改动之后的源码可以参考 iverilog-v/tgt-json当中的代码

- 定义文件指针
  ```c
    /*
    *		iverilog/tgt-vvp/vvp.c
    */

    FILE*vvp_out = 0;
    int vvp_errors = 0;
    unsigned show_file_line = 0;
  ```



- 从命令行获取输出文件信息，打开文件

  ```c
  #ifdef HAVE_FOPEN64
        vvp_out = fopen64(path, "w");
  #else
        vvp_out = fopen(path, "w");
  #endif
        if (vvp_out == 0) {
  	    perror(path);
  	    return -1;
        }
  ```



- 输出vvp文件到关闭文件指针主要调用函数

    ```c
      vvp_errors = 0;

      draw_execute_header(des);

      fprintf(vvp_out, ":ivl_delay_selection \"%s\";\n",
                       ivl_design_delay_sel(des));

      { int pre = ivl_design_time_precision(des);
	char sign = '+';
	if (pre < 0) {
	      pre = -pre;
	      sign = '-';
	}
	fprintf(vvp_out, ":vpi_time_precision %c %d;\n", sign, pre);
      }

      draw_module_declarations(des);

        /* This causes all structural records to be drawn. */
      ivl_design_roots(des, &roots, &nroots);
      for (i = 0; i < nroots; i++)
	    draw_scope(roots[i], 0);

        /* Finish up any modpaths that are not yet emitted. */
      cleanup_modpath();

      rc = ivl_design_process(des, draw_process, 0);

        /* Dump the file name table. */
      size = ivl_file_table_size();
      fprintf(vvp_out, "# The file index is used to find the file name in "
                       "the following table.\n:file_names %u;\n", size);
      for (idx = 0; idx < size; idx++) {
	    fprintf(vvp_out, "    \"%s\";\n", ivl_file_table_item(idx));
      }

      fclose(vvp_out);
    ```

**注：以上代码是tgt-vvp当中的核心部分，因此要改写为自己tgt-json组件需要从大致这三个地方进行改动，具体详细的改动请参考源码 iverilog-v/tgt-json代码。**

## tgt-json组件与iverilog源码进行整合

开发完tgt-json组件之后，则需要把组件加入到iverilog源码当中。因此除了保证功能上面的可用性还要保证编译，安装等步骤与没加入tgt-json组件iverilog源码一样。为此就需要分析iverilog的预编译文件进行相应的修改，大体思路是参考其它可加载代码生成器组件进行tgt-json组件的嵌入工作。为此，下面将会详细地说明对于iverilog当中预编译文件修改的地方。

### iverilog编译器核心与tgt-vvp组件调用关系

#### 用户命令行的解析

在iverilog当中根据官方使用手册（文档为：iverilog.pdf位于安装目录下），iverilog除了可以输出vvp文件还有null,fpga,vhdl三种输出模式。至于选择哪一种是根据输入的命令行参数进行解析之后iverilog进行选择性地输出。参考了iverilog的使用手册，我们知道如果要让iverilog输出vhdl，那么命令行应该输入的命令为：

```powershell
iverilog -t vhdl  -o "target_file.vhdl" file.v file.v
```

那么我们可以知道如果要输出json格式的数据命令参数应该为：

```powershell
iverilog -t json  -o "target_file.json" file.v file.v
```

其中-o后面的参数是生成目标的文件名，这个文件名会被解析然后传递给 tgt-vvp当中的文件指针用于产生一个vvp文件。-t后面的参数为选择参数，用于选择iverilog输出文件的类型。

由iverilog开发文档可知，对于命令行的参数的解析是由iverilog当中的driver进行解析的。因此我们需要知道driver当中是如何解析命令并且调用相应的代码生成器。

```c
/*
*	iverilog/driver/main.c
*/

while ((opt = getopt(argc, argv, "B:c:D:d:Ef:g:hl:I:iL:M:m:N:o:P:p:Ss:T:t:uvVW:y:Y:")) != EOF) {

switch (opt) {
	case 'B':
    /* The individual components can be located by a
		 single base, or by individual bases. The first
		 character of the path indicates which path the
		 user is specifying. */
        switch (optarg[0]) {
            case 'M': /* Path for the VPI modules */
                vpi_dir = optarg+1;
                break;
            case 'P': /* Path for the ivlpp preprocessor */
                ivlpp_dir = optarg+1;
                break;
            case 'V': /* Path for the vhdlpp VHDL processor */
                vhdlpp_dir = optarg+1;
                break;
            default: /* Otherwise, this is a default base. */
                base=optarg;
                break;
        }
        break;
    case 'c':
    case 'f':
        add_cmd_file(optarg);
        break;	
        /*	                      ......                     */
    case 't':
        targ = optarg;
        break;
```

这一段代码是driver预处理器下main.c的代码，由getopt可以知道这一段代码本质上是对用户命令行参数的解析

>​	函数说明 getopt()用来分析命令行参数。参数argc和argv分别代表参数个数和内容，跟main（）函数的命令行参数是一样的。参数 optstring为选项字符串， 告知 getopt()可以处理哪个选项以及哪个选项需要参数，如果选项字符串里的字母后接着冒号“:”，则表示还有相关的参数，全域变量optarg 即会指向此额外参数。如果在处理期间遇到了不符合optstring指定的其他选项getopt()将显示一个错误消息，并将全域变量optopt设为“?”字符，如果不希望getopt()打印出错信息，则只要将全域变量opterr设为0即可。

根据`getopt`函数可以知道，当解析到-t参数的时候iverilog会记录后面的参数，例如如果输入的命令是：`iverilog -t vhdl  -o "target_file.vhdl" file.v file.v`那么`targ`就会记录用户选择了vhdl模式文本输出。

#### 调用关系

由于iverilog对于tgt-*系列代码生成器的调用原理是：iverilog编译的过程中将系列代码生成器编译为动态链接库，然后存放在安装目录下。一旦选择了其他的输出模式则会调用相应的动态链接库(详细调用过程请参考iverilog代码框架说明文档)。因此在开发tgt-json组件的时候只要让tgt-json组件经过编译之后生成的动态链接库能存放在安装目录下以供iverilog需要用到时调用即可。因此tgt-json组件与iverilog的兼容问题就主要在修改预编译文件Makefile系列文件上面

### iverilog预编译文件的修改

对于iverilog的编译过程分析具体说明可以参考[iverilog核心组件移植说明文档](https://good.ncu.edu.cn/gitlab/pengxin/iverilog-ncu/-/blob/master/README.md)。我们这里只是简述大概需求修改哪些预编译文件

- 将tgt-json加入 iverilog/Makefile.in 该行代码中

  ```Makefile
    SUBDIRS = ivlpp vhdlpp vvp vpi libveriuser cadpli tgt-json tgt-null 	tgt-stub tgt-vvp \tgt-vhdl tgt-vlog95 tgt-pcb tgt-blif tgt-sizer driver
  ```



- 修改 iverilog/driver/iverilog.man.in，加入下面的代码在336行

  ```
  .TP 8
  .B json
  This is an option to generate \fBjson\fP data format. In iverilog, vvp files
  are generated by default, but \fBjson\fP data format can be selected. The
  code generation of \fBjson\fP data format is roughly the same as that of
  vvp file. The data in \fBjson\fP format records the main information in the
  vvp file information, so it is more lightweight to process.
  ```

由于增加了一个输出json的功能，除了改变相应的预编译文件，为了后期使用的方便开发过程中自己写了相应的使用说明，到时候使用的时候可以直接用编译生成的使用手册，使得后期使用的时候用起来更加地方便。



- 改写tgt-vvp下的Makefile.in预编译文件	

因为tgt-json组件的研发思路大部分是参考tgt-vvp，所以只要以tgt-vvp组件下面的预编译文件Makefile.in修改即可得到tgt-json组件的预编译文件。因为改的地方较多可以直接去参考改好的tgt-json组件下的Makefile.in