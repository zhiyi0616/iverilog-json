#include <stdio.h>
#include <stdlib.h>
# include "json_priv.h"



/*------------------------ .net -----------------------------------------------------------------------*/
typedef struct Net_Data
{
    char statements[SIZE_DATA];
    char label[SIZE_DATA];
    char opcode[SIZE_DATA];
    char name[SIZE_DATA];

    struct Net_Data* next;
    
}Net_Data;


Net_Data* net_head = NULL;
Net_Data* net_end = NULL;

Net_Data* Net_CreateNode(char* label, char* opcode, char* name, char* statements);


/*-------variable---------------------------------*/
typedef struct Var_Data
{
    char statements[SIZE_DATA];
    char label[SIZE_DATA];
    char opcode[SIZE_DATA];
    char name[SIZE_DATA];

    struct Var_Data* next;
    
}Var_Data;


Var_Data* var_head = NULL;
Var_Data* var_end = NULL;


Var_Data* Var_CreateNode(char* label, char* opcode, char* name, char* statements);
/*--------------------------------------------------*/


/*------------------ cells -------------------------------------*/

typedef struct Cells_Datas
{   
    char label[SIZE_DATA];
    char type[SIZE_DATA];
    char opcode[SIZE_DATA];
    char operand[SIZE_DATA];
}Cells_Datas;


// 定义一个结构体用于存储 "cells"的JSON信息链表结点
typedef struct Cells_Node
{

    char statements[SIZE_DATA];
    Cells_Datas cells;

    struct Cells_Node* next;

}Cells_Node;

Cells_Node* Cell_CreateNode(char* label, char* type, const char* opcode, char* operand, char* statements);


Cells_Node* cell_head= NULL;
Cells_Node* cell_end = NULL;


// 把一个字符串赋值给另一个字符数组
void receive_string(char* des_str, const char* sour_str)
{
    int icount =0;
    while(*sour_str)
    {
        des_str[icount] = *sour_str;
        sour_str++;
        icount++; 
    }
    des_str[icount] = '\0';
}

// 读取操作函数
void Read_ListData(void)
{
    
    cJSON *arry_elements = cJSON_CreateArray();
    cJSON *root = cJSON_CreateObject();


    Cells_Node *node = cell_head;
    char* ptr = NULL;

    if (node == NULL)
    {
        printf("List is empyt！\n");
    }
    else
    {
        for (int index = 0; index < Cell_GetListSize(); index++)
        {
            cJSON *elements;
            elements = cJSON_CreateObject(); 

            cJSON_AddStringToObject(elements, "statements:", node->statements);
            cJSON_AddStringToObject(elements, "label", node->cells.label);
            cJSON_AddStringToObject(elements, "type", node->cells.type);
            cJSON_AddStringToObject(elements, "opcode", node->cells.opcode);
            cJSON_AddStringToObject(elements, "operand", node->cells.operand);

            cJSON_AddItemToArray(arry_elements, elements);
            node = node->next;
        }
    }

    cJSON_AddItemToObject(root, "cells", arry_elements);


/*---------------------.var----------------------------------------------------*/

    Var_Data *node_var = var_head;
    cJSON *arry_elements_var = cJSON_CreateArray();

    if (node_var == NULL)
    {
        printf("List is empyt！\n");
    }
    else
    {
        for (int index = 0; index < Var_GetListSize(); index++)
        {

            cJSON *elements_var;
            elements_var = cJSON_CreateObject(); 

            cJSON_AddStringToObject(elements_var, "statements:", node_var->statements);
            cJSON_AddStringToObject(elements_var, "label:", node_var->label);
            cJSON_AddStringToObject(elements_var, "opcode:", node_var->opcode);
            cJSON_AddStringToObject(elements_var, "name:", node_var->name);

            cJSON_AddItemToArray(arry_elements_var, elements_var);
            node_var = node_var->next;
        }
    }

    cJSON_AddItemToObject(root, "variable", arry_elements_var);


/*---------------------------------- .net -----------------------------------------*/
    Net_Data *node_net = net_head;
    cJSON *arry_elements_net = cJSON_CreateArray();

    if (node_net == NULL)
    {
        printf("List is empyt！\n");
    }
    else
    {
        for (int index = 0; index < Net_GetListSize(); index++)
        {

            cJSON *elements_net;
            elements_net = cJSON_CreateObject(); 

            cJSON_AddStringToObject(elements_net, "statements:", node_net->statements);
            cJSON_AddStringToObject(elements_net, "label:", node_net->label);
            cJSON_AddStringToObject(elements_net, "opcode:", node_net->opcode);
            cJSON_AddStringToObject(elements_net, "name:", node_net->name);

            cJSON_AddItemToArray(arry_elements_net, elements_net);
            node_net = node_net->next;
        }
    }
    
    cJSON_AddItemToObject(root, "net", arry_elements_net);
    
    ptr = cJSON_Print(root);
    fprintf(vvp_out_JSON, ptr);
    cJSON_Delete(root);

}


/*----------------cells 链表操作函数----------------------------------------------------*/


// 删除整个链表
void Cell_DeleteList(void)
{
    Cells_Node *node = cell_head;

    if (node == NULL)
    {
        return;
    }

    while (node != NULL)
    {
        Cells_Node *temp = node;
        node = node->next;
        free(temp);
    }

    cell_head = NULL;
    cell_end = NULL;
        
}

// 创建结点
Cells_Node* Cell_CreateNode(char* label, char* type, const char* opcode, char* operand, char* statements)
{
    Cells_Node *node = (Cells_Node *)malloc(sizeof(Cells_Node));
    receive_string(node->cells.label, label);
    receive_string(node->cells.type, type);
    receive_string(node->cells.opcode, opcode);
    receive_string(node->cells.operand, operand);

    receive_string(node->statements, statements);

    node->next = NULL;

    return node;
}          



// 增加结点
void Cell_AddNode(char* label, const char* opcode, char* type, char* operand, char* statements)
{
    Cells_Node *node = Cell_CreateNode(label, type, opcode, operand, statements);

    if (cell_head == NULL)   //链表为空
    {
        cell_head = node;
    }
    else  //链表已存在节点
    {
        cell_end->next = node;
    }
    
    cell_end = node;
}       


// 获得链表的大小
unsigned int  Cell_GetListSize(void)
{
    Cells_Node *node = cell_head;
    unsigned int size = 0;

    while (node != NULL)
    {
        node = node->next;
        size++;
    }
    
    return size;
}


/*-------------------- variable -----------------------------------------------------------*/

// .var创建结点
Var_Data* Var_CreateNode(char* label, char* opcode, char* name, char* statements)
{
    Var_Data *node = (Var_Data *)malloc(sizeof(Var_Data));
    receive_string(node->label, label);
    receive_string(node->opcode, opcode);
    receive_string(node->name, name);
    receive_string(node->statements, statements);
    


    node->next = NULL;

    return node;
}    


//.var 增加结点
void Var_AddNode(char* label, char* opcode,  char* name, char* statements)
{
    Var_Data *node = Var_CreateNode(label, opcode, name, statements);

    if (var_head == NULL)   //链表为空
    {
        var_head = node;
    }
    else  //链表已存在节点
    {
        var_end->next = node;
    }

    var_end = node;

}       

// 删除整个链表
void Var_DeleteList(void)
{
    Var_Data *node = var_head;

    if (node == NULL)
    {
        return;
    }

    while (node != NULL)
    {
        Var_Data *temp = node;
        node = node->next;
        free(temp);
    }

    var_head = NULL;
    var_end = NULL;
        
}

// 获得链表的大小
unsigned int  Var_GetListSize(void)
{
    Var_Data *node = var_head;
    unsigned int size = 0;

    while (node != NULL)
    {
        node = node->next;
        size++;
    }
    
    return size;
}  

/*-------------------------- .net ---------------------------------------------------*/


// .var创建结点
Net_Data* Net_CreateNode(char* label, char* opcode, char* name, char* statements)
{
    Net_Data *node = (Net_Data *)malloc(sizeof(Net_Data));
    receive_string(node->label, label);
    receive_string(node->opcode, opcode);
    receive_string(node->name, name);
    receive_string(node->statements, statements);
    
    node->next = NULL;

    return node;
}    




//.var 增加结点
void Net_AddNode(char* label, char* opcode,  char* name, char* statements)
{
    Net_Data *node = Net_CreateNode(label, opcode, name, statements);

    if (net_head == NULL)   //链表为空
    {
        net_head = node;
    }
    else  //链表已存在节点
    {
        net_end->next = node;
    }

    net_end = node;
}       


// 删除整个链表
void Net_DeleteList(void)
{
    Net_Data *node = net_head;

    if (node == NULL)
    {
        return;
    }

    while (node != NULL)
    {
        Net_Data *temp = node;
        node = node->next;
        free(temp);
    }

    net_head = NULL;
    net_end = NULL;
        
}

// 获得链表的大小
unsigned int  Net_GetListSize(void)
{
    Net_Data *node = net_head;
    unsigned int size = 0;

    while (node != NULL)
    {
        node = node->next;
        size++;
    }
    
    return size;
}  


// 释放链表的存储空间，以免一直占用过大的内存空间
void Free_List(void)
{
    Cell_DeleteList();
    Var_DeleteList();
    Net_DeleteList();
}


// 处理json和vvp选择函数, 返回值0则证明此次输出要输出json数据格式
int handle_fileformat(char* path)
{
    int icount = 0;
    int icout_file = 0;
    int flag = 0;
    int flag_file = 1;
    char* pt = path;
    char temp_filename[10];
    for(icount = 0; pt[icount] != '\0'; icount++)
    {
        // 指针移动到了文件后缀处.
        if ((pt[icount] == '.') || flag)
        {
            flag = 1;
            temp_filename[icout_file] = pt[icount];
            icout_file++;

        }
    }

    flag_file = strcmp(temp_filename, ".json");

    return flag_file;

}

void splice_jsonfile(char* path, char* des_path)
{

    int i = 0;
    char json_filename[] = ".json";
    char temple_filename[SIZE_DATA];
    char* pt = path;

    for(i = 0; pt[i] != '\0'; i++)
    {
        if(pt[i] == '.')
            break;

        temple_filename[i] = pt[i];
    }

    strcat(temple_filename, ".json");

    receive_string(des_path, temple_filename);
}


void splice_vvpfile(char* path, char* des_path)
{

    int i = 0;
    char json_filename[] = ".vvp";
    char temple_filename[SIZE_DATA];
    char* pt = path;

    for(i = 0; pt[i] != '\0'; i++)
    {
        if(pt[i] == '.')
            break;

        temple_filename[i] = pt[i];
    }

    strcat(temple_filename, ".vvp");

    receive_string(des_path, temple_filename);
}

int
fprintf_selfmade(FILE* stream, const char* format, ...)
{

}